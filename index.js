// We can access or retreive or manipulate a specific element using DOM

const txtFirstName = document.querySelector('#txt-first-name');

const spanFullName = document.querySelector('#span-full-name');

// Alternative ways to access elements

/*
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByTagName('input');
*/

// Targetting our input element or text element
// Document refers to our whole webpage, whole webpage document
// querySelector - used to select a specific object from our document webpage, it actually takes a string input that takes in just like a css.

// Targetting our element or access an element based on it's id by different methods

// querySelector is flexible as by one function, we can access an element unlike by Id, class, tag.

// We have something called event listeners..everytime a user clicks a button or inputs a text using keyboard, we use events..
// For us to perform an event, when we interact, we need it to listen to us.

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
	// innerHTML is the property
	// keyup is actually the event
})

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target); // shows where the event is listening to or pointing out to
	console.log(event.target.value); // Shows the change in values in the console..stores the previous ones also even though you backspace the whole thing..
})